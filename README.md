# VAW

VAW (**V**K **A**PI **W**rapper) is a high-level wrapper for good low-level [wrapper](https://github.com/python273/vk_api) for [shit](https://vk.com/dev/methods).

## Installation

	pip install vaw

## Code example

	from vaw import log_in, user_by_url

	log_in(VK_LOGIN, VK_PASSWORD)
	mrbirdman = user_by_url('https://vk.com/kostyakolesnyak')
	mrbirdman.send('Hi from vaw!')

## Docs

> Documentation is cheap. Show me the [code](https://github.com/mrbirdman2000/vk_api_wrap/blob/master/vaw.py
)!

• *Linus Torvalds*

## License

GNU General Public License.

## PS

I don't like PEP very much, so sorry for IDE's warnings.
